def stevilocrnih(matrika):
    """Vrne število, ki pove, koliko mora biti črnih polj v rešeni uganki."""
    beli = 0
    for vrstica in matrika: # za vsako cifro v matriki
        for element in vrstica:
            if element > 0:
                beli += element # jo dodaj k belim
    crni = (len(matrika)*len(matrika[0])) - beli # število črnih je število vseh minus število belih
    return crni

def sosedi(matrika,tocka,x=None):
    """Vrne seznam točk z vrednostjo "x", ki se dotikajo točke "tocka". Če "x" ni podana, vrne vse dotikajoče."""
    i,j = tocka
    seznam = []
    mozne = [(i+1,j),(i-1,j),(i,j+1),(i,j-1)]
    for m,n in mozne:
        if m >= 0 and m < len(matrika) and n >= 0 and n < len(matrika[0]):
            if x is None:
                seznam.append((m,n))
            elif matrika[m][n] == x: # pogledamo če je njena vrednost enaka "x"
                seznam.append((m,n)) # če da, pripnemo k seznamu
    return set(seznam)

def otok(matrika,tocka,x):
    """Vrne seznam točk z vrednostjo "x", ki so povezane s točko "tocka", vključno z njo."""
    i,j = tocka # koordinate točke
    povezani = [tocka] # v ta seznam se shranjujejo vse povezane točke vključno z začetno
    nepreverjeni = [tocka] # v tem seznamu so vsi, ki jim je treba preveriti sosede
    while len(nepreverjeni) > 0: # dokler imamo še kaj nepreverjenih sosed
        novi = [] # vsi novi, ki jih pridobimo v tej zanki
        for i in nepreverjeni: # za vsakega do zdaj nepreverjenega preverimo sosede
            for t in sosedi(matrika,i,x): # za vsako sosedo pogledamo, če je že v seznamu povezanih
                if t not in povezani: # če ni v seznamu, jo dodamo med povezane in med nove
                    povezani.append(t)
                    novi.append(t)
        nepreverjeni = novi # ko preverimo vse nepreverjene, jih zamenjamo z novimi
    return set(povezani)

def dvakratdva(matrika,i,j):
    """Vrne True, ce je polje (i,j) zgornji levi vogal 2 x 2 crnega bloka."""
    return ((i != len(matrika) - 1) and # nismo na desnem robu
            (j != len(matrika[0]) - 1) and # nismo na spodnjem robu
            matrika[i  ][j  ] == -1 and
            matrika[i+1][j  ] == -1 and
            matrika[i  ][j+1] == -1 and
            matrika[i+1][j+1] == -1)

def pravilna(matrika):
    preverili_crne = False
    for i in range(len(matrika)):
        for j in range(len(matrika[0])):
            if matrika[i][j] == -1 and not preverili_crne:
                preverili_crne = True
                if len(otok(matrika,(i,j),-1)) != stevilocrnih(matrika):
                    return False
            if dvakratdva(matrika,i,j):
                return False
            if matrika[i][j] > 0 and len(otok(matrika,(i,j),0)) != matrika[i][j]:
                return False
    return True
