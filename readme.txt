﻿Projekt igrica pri predmetu Programiranje 2.

Opis igrice:

Igrica Nurikabe je znana tudi kot "Cell Structure" in "Islands in the Stream".
Igra se igra na mreži, v kateri nekatera polja vsebujejo številko.
Cilj igre je s črno pobarvati toliko praznih polj, da bo zadoščeno naslednjim pravilom:

 * vsa črna polja morajo biti povezana
 * nikjer v mreži ne sme biti črnih 2x2 polj
 * vsako polje s številko mora biti belo in mora biti del povezanih belih polj, ki jih imenujemo "otok"
 * vsak otok ima toliko povezanih belih polj, kakršna je številka, vsebovana v otoku
 * vsak otok vsebuje samo eno številko
 * dva otoka ne smeta biti povezana

Velja, da sta polji povezani, če se dotikata s katero od stranic (če se dotikata v kotu, nista povezani).

Opisi datotek:

* generator_igre.py vsebuje funkcije, ki generirajo novo igro oz. matriko s številkami in jo zapišejo v .txt datoteko
* preveri_resitev.py vsebuje funkcije, ki preverjajo, ali je dan nurikabe pravilno rešen
* beri_pisi_txt.py vsebuje funkcije za zapis/branje matrik iz .txt datotek ter funkcijo za brisanje .txt datotek
* nurikabe.py nariše glavno okno v katerem se izvaja igra
* mini.txt, midi.txt, maxi.txt so trije že generirani nurikabeji
