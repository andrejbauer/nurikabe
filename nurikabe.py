from tkinter import *
from beri_pisi_txt import *
from preveri_resitev import *
from generator_igre import generiraj_majhne_otoke

class Nurikabe():

    def __init__(self,master):

        # Glavni menu
        menu = Menu(master)
        master.config(menu=menu)

        igra_menu = Menu(menu)
        menu.add_cascade(label='Igra',menu=igra_menu)
        igra_menu.add_command(label='5x5',command=lambda: self.zacni_igro(5,5))
        igra_menu.add_command(label='10x10',command=lambda: self.zacni_igro(10,10))
        igra_menu.add_command(label='15x15',command=lambda: self.zacni_igro(15,15))

        # Igralno polje
        self.canvas = Canvas(master,width=500,height=500)
        self.canvas.grid(row=1,column=0)
        self.canvas.bind('<Button-1>', self.klikLeva)
        self.canvas.bind('<Button-2>', self.klikDesna)
        self.canvas.bind('<Button-3>', self.klikDesna)
        self.gumb_preveri = Button(master,text='Preveri me!',command=self.preveri)
        self.gumb_preveri.grid(row=0,column=0)

    def zacni_igro(self,a,b):
        """Začni igro velikosti a x b."""
        # generiraj nurikabe
        crne = generiraj_majhne_otoke(a,b)
        self.matrika = [[(-1 if ((i,j) in crne) else 0) for i in range(a)] for j in range(b)]
        # tu bi morali v self.resena vpisati stevilke, zaenkrat samo prekopiramo
        self.resena = [[polje for polje in vrstica] for vrstica in self.matrika]

        # narisi začetno stanje

        # zbriši vse, kar je trenutno narisano
        self.canvas.delete(ALL)

        # nastavi velikost canvasa
        self.canvas.config(width=50*a, height=50*b)

        # nariši črte
        for i in range(50,a*50,50):
            self.canvas.create_line(i,a*50,i,0)
        for j in range(50,b*50,50):
            self.canvas.create_line(b*50,j,0,j)
        # nariši črna polja in številke
        for i in range(len(self.matrika)):
            for j in range(len(self.matrika[0])):
                if self.matrika[i][j] > 0:
                    self.canvas.create_text(25 + j*50,25 + i*50,text=self.matrika[i][j])
                elif self.matrika[i][j] == -1:
                    self.pobarvaj_crno(i,j)

    def preveri(self):
        print(pravilna(self.resena))

    def klikLeva(self, event):
        j = int(event.x / 50)
        i = int(event.y / 50)
        if  self.matrika[i][j] == 0:
            self.pobarvaj_crno(i,j)
            self.resena[i][j] = -1
        else:
            pass

    def klikDesna(self, event):
        j = int(event.x / 50)
        i = int(event.y / 50)
        if  self.matrika[i][j] == 0:
            self.pobarvaj_belo(i,j)
            self.resena[i][j] = 0
        else:
            pass

    def pobarvaj_crno(self, i, j):
        x = j * 50
        y = i * 50
        self.canvas.create_rectangle(x, y, x+50, y+50, fill="black")

    def pobarvaj_belo(self, i, j):
        x = j * 50
        y = i * 50
        self.canvas.create_rectangle(x, y, x+50, y+50, fill="white")

root = Tk()
aplikacija = Nurikabe(root)
root.title('Nurikabe')
root.mainloop()
