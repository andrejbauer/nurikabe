import random
from beri_pisi_txt import *
from preveri_resitev import *

def generiraj_majhne_otoke(a,b):
    """Izmed kandidatov za naslednje črno polje izberi takega, ki nastopa v prevelikem
       otoku. Otok je prevelik, če je večji od max(a,b)."""
    # sproti računamo matriko
    matrika = [[0 for i in range(a)] for j in range(b)]
    # največja dopustna velikost otoka
    max_otok = max(a,b)
    # začetno črno polje
    x = random.randint(0, a-1)
    y = random.randint(0, b-1)
    matrika[x][y] = -1
    crne = set([(x,y)])
    robovi = []
    while True:
        kandidati = set()
        # dodamo vse sosede črnih med kandidate
        for (i,j) in crne:
            for k,l in [(i+1,j),(i-1,j),(i,j+1),(i,j-1)]:
                if k >= 0 and k < a and l >= 0 and l < b and (k,l) not in crne:
                    kandidati.update([(k,l)])
        # odstranimo kandidate, ki bi povzročili 2 x 2 črn blok
        kandidati = kandidati - prepovedane_bele(a,b,crne)
        # ohranimo kandidate, ki nastopajo v prevelikih otokih
        kandidati = [k for k in kandidati if len(otok(matrika, k, 0)) > max_otok]
        if len(kandidati) == 0:
            # Vsi otoki so majhni, lahko koncamo
            return crne
        else:
            # izberemo enega naključno in ga dodamo med črne
            m,n = random.choice(list(kandidati))
            matrika[m][n] = -1
            crne.update([(m,n)])

def generiraj_do_robov(a,b):
    """Generiramo toliko časa, da se dotaknemo vseh štirih robov s črno barvo."""
    # začetno črno polje
    x = random.randint(0, a-1)
    y = random.randint(0, b-1)
    crne = set([(x,y)])
    bele = set([(i,j) for i in range(a) for j in range(b)]) - crne
    robovi = []
    while len(robovi) < 4: #len(bele) > 1 or
        bele = set()
        for (i,j) in crne:
            for k,l in [(i+1,j),(i-1,j),(i,j+1),(i,j-1)]:
                if k >= 0 and k < a and l >= 0 and l < b and (k,l) not in crne:
                    bele.update([(k,l)])
        bele = bele - prepovedane_bele(a,b,crne)
        m,n = random.choice(list(bele))
        if m == 0 and 1 not in robovi:
            robovi.append(1)
        elif m == (a-1) and 2 not in robovi:
            robovi.append(2)
        if n == 0 and 3 not in robovi:
            robovi.append(3)
        elif n == (b-1) and 4 not in robovi:
            robovi.append(4)
        crne.update([(m,n)])
    return crne

def prepovedane_bele(a,b,crne):
    """Vrni množico vseh belih polj, ki bi povzročili črn blok 2 x 2, če bi jih pobarvali črno."""
    nemogoce = set()
    for i in range(a-1):
        for j in range(b-1):
            stej = 0
            bele = []
            for s in [(i,j),(i,j+1),(i+1,j),(i+1,j+1)]:
                if s in crne:
                    stej += 1
                else:
                    bele.append(s)
            if stej == 3:
                nemogoce.update(bele)
    return nemogoce

# a = 7 # stevilo vrstic
# b = 7 # stevilo stolpcev
# m = [[0 for i in range(a)]for j in range(b)]
# for i,j in generiraj_majhne_otoke(a,b):
#     m[i][j] = -1

# zapisi('p1.txt',m)
