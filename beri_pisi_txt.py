import os

def matrika(niz):
    return list(map(int,niz.strip().split(",")))

def nalozi(ime):
    m = []
    with open(ime,'r') as f:
        for vrstica in f:            
            m.append(matrika(vrstica.strip()))
    return m

def zapisi(ime,puzzle):
    with open(ime,'w') as f:
        for vrstica in puzzle:
            print(','.join(str(element) for element in vrstica), file=f)

def zbrisi(ime):
    os.remove(ime)
